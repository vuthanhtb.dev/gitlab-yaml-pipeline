### Install Gitlab-Runner on VM Ubuntu 20.04 LTS

```sh
sudo curl -L --output /usr/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64"
```

```sh
sudo chmod +x /usr/bin/gitlab-runner
```

```sh
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
```

```sh
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
```

```sh
sudo gitlab-runner start
```

```sh
sudo gitlab-runner register
```

```sh
rm /home/gitlab-runner/.bash_logout
```

### Install Docker

```sh
adduser gitlab-runner sudo
```

```
vi /etc/sudoers

#Add below line inside /etc/sudoers file and save
gitlab-runner ALL=(ALL) NOPASSWD:ALL
```

```sh
apt-get -y update
```

```sh
apt-get -y install docker
```

```sh
apt-get -y install docker.io
```
